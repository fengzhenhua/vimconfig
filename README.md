# InstallVim.sh #

## 安装vim配置 ##

下载本脚本后，直接执行命令：
```bash  
./InstallVim.sh
```

## 变更历史 ##

- 2023年07月07日 将vim配置单独写成脚本，放在gitlab中统一管理 ,版本号V1.7
