" ====vimrc====
" Version : v1.7
" Author  : Feng Zhenhua
" Email   : fengzhenhua@outlook.com
"
" History :
" 2023年07月07日 配合单独脚本，合并到gitlab中管理，以git@github.com协议取代https://github.com
"
" 基础设置
set nocompatible              " be iMproved, required
filetype off                  " required
filetype plugin on
set grepprg=grep\ -nH\ $*
filetype indent on
let g:tex_flavor='latex'      "解决打开空白tex文档时，latex-suite不能启动的问题
syntax enable
set bg=dark
set nu
"设置配色方案
set termguicolors  "开启真彩
"colorscheme murphy
"colorscheme pablo
"--------------------设置第三方配色主题--------------------
"colorscheme molokai 
"let g:molokai_original = 1
"let g:rehash256 = 1
"设置列高亮
set cursorcolumn
"设置行线
set cursorline
"显示未执行的命令
set showcmd
"设置自动备份
"set backup
"set backupext=.bak
"保存一个原始文件
"set patchmode=.orig
"设置用命令行版stardict(sdcv)查询单词
"set keywordprg=sdcv
"设定字符编码20180320加入
set encoding=utf-8
set fileencodings=utf-8,cp936
set termencoding=utf-8
"设置缩进
set autoindent shiftwidth=3
"设定软tab键,用来产生四个空格20180323
set softtabstop=4
"取消vim编辑时滴滴声
set noeb vb t_vb= 
"拼写检查
" ]s 将光标移到下一个拼写错误处 ， [s 将光标移到上一下拼写错误
" zg 将单词加入词典 zug 撤销将单词加入词典
" z= 拼写建议
set nospell
"set spell
"
"##### Vundle ###########
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'git@github.com:VundleVim/Vundle.vim'
Plugin 'git@github.com:vim-latex/vim-latex'
Plugin 'git@github.com:aperezdc/vim-template'
Plugin 'git@github.com:SirVer/ultisnips'
Plugin 'git@github.com:honza/vim-snippets'
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"
" 简明帮助
" :PluginList       - 列出配置过的插件
" :PluginInstall    - 安装插件; 附加`!` 更新插件，或只用:PluginUpdate
" :PluginSearch foo - 查找 foo; 附加`!` 刷新本地缓存
" :PluginClean      - 确认删除无用的插件; 附加`!`强制删除
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"##### Vundle end  ###########
"
"##### vim-latex/latex-suite  ###########
"打开dtx文件自定义命令
autocmd BufNewFile,BufRead *.dtx source ~/.vim/bundle/vim-latex/ftplugin/dtx.vim
let g:Tex_Flavor='latex'
" 控制编译过程中的警告信息
let g:Tex_IgnoredWarnings = 
	\'Underfull'."\n".
	\'Overfull'."\n".
	\'specifier changed to'."\n".
	\'You have requested'."\n".
	\'Missing number, treated as zero.'."\n".
	\'There were undefined references'."\n".
	\'Citation %.%# undefined'."\n".
	\"LaTeX hooks Warning"
let g:Tex_IgnoreLevel = 8
let g:Tex_GotoError = 0
" 控制编译方式和PDF浏览软件manjaro linux
let g:Tex_DefaultTargetFormat = 'pdf'
let g:Tex_CompileRule_pdf = 'xelatex -synctex=1 -interaction=nonstopmode -file-line-error-style $*'
let g:Tex_ViewRule_pdf='evince'
"##### vim-latex/latex-suite end  ###########
"
"##### vim-templates ###########
" 自定义模板所在文件夹
let g:templates_directory = '~/.vim/templates'
let g:templates_user_variables = [['EMAIL', 'GetEmail'],['AUTHOR', 'GetAuthor']]
function GetEmail()
   return 'fengzhenhua@outlook.com'
endfunction
function GetAuthor()
   return '冯振华'
endfunction
"##### vim-templates  end ###########
"
"##### auto fcitx5  ###########
let g:input_toggle = 0
function! Fcitx2en()
   let s:input_status = system("fcitx5-remote")
   if s:input_status == 2
      let g:input_toggle = 1
      let l:a = system("fcitx5-remote -c")
   endif
endfunction

function! Fcitx2zh()
   let s:input_status = system("fcitx5-remote")
   if s:input_status != 2 && g:input_toggle == 1
      let l:a = system("fcitx5-remote -o")
      let g:input_toggle = 0
   endif
endfunction

set ttimeoutlen=150
"退出插入模式
"autocmd InsertLeave * call Fcitx2en()  "如果需要使用Fcitx5请取消前面的注释
"进入插入模式
"autocmd InsertEnter * call Fcitx2zh() "如果需要使用Fcitx5请取消前面的注释
"##### auto fcitx end ######
" 设置ibus 自动切换输入法
let g:ibus#layout = 'xkb:us::eng'
let g:ibus#engine = 'table:wubi-jidian86'
"
" 键位绑定，方便使用vim编辑md文件时快速预览
"##### markdown-preview.vim ######
nmap <silent> <F8> <Plug>MarkdownPreview        " 普通模式
imap <silent> <F8> <Plug>MarkdownPreview        " 插入模式
nmap <silent> <F9> <Plug>StopMarkdownPreview    " 普通模式
imap <silent> <F9> <Plug>StopMarkdownPreview    " 插入模式
"##### markdown-preview.vim end  ######
"
"Set  F5 to run and compile all language program
map <F5> :call  CompileRunGcc()<CR>
func! CompileRunGcc()
   exec "w"
   if &filetype == 'sh'
      :!time bash %
   elseif &filetype == 'python'
      exec "!time python3 %"
   elseif &filetype == 'c'
      exec "!g++ % -o %<"
      exec "!time ./%<"
   elseif &filetype == 'cpp'
      exec "!g++ % -o %<"
      exec "!time ./%<"
   elseif &filetype == 'java'
      exec "!javac %"
      exec "!time java %<"
   elseif &filetype == 'html'
      exec "!firefox % &"
   elseif &filetype == 'go'
      exec "!go build %<"
      exec "!time go run %"
   elseif &filetype == 'mkd'
      exec "!~/.vim/markdown.pl % > %.html &"
      exec "!firefox %.html &"
   endif
endfunc
" set for vim-auto-popmenu
" enable this plugin for filetypes, '*' for all files.
"let g:apc_enable_ft = {'text':1, 'markdown':1, 'php':1}
let g:apc_enable_ft = {'*':1}

" source for dictionary, current or other loaded buffers, see ':help cpt'
set cpt=.,k,w,b

" don't select the first item.
set completeopt=menu,menuone,noselect

" suppress annoy messages.
set shortmess+=c
