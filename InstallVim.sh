#! /bin/sh
#
# Program  : InstallVim.sh
# Version  : V1.0
# Author   : fengzhenhua
# Email    : fengzhenhua@outlook.com
# CopyRight: Copyright (C) 2022-2025 FengZhenhua(冯振华)
# License  : Distributed under terms of the MIT license.
# Distributed under terms of the MIT license.

# 设置变量
VimVundle="/home/$USER/.vim/bundle/Vundle.vim"
VimSnip="/home/$USER/.vim/bundle/vim-snippets/UltiSnips/"
# 定义函数
ArchInstall(){
   sudo pacman -S --needed --noconfirm $1 &> /dev/null
   return 0
}
# 安装vim
ArchInstall vim
ArchInstall git
# 安装Vundle插件程序
if [ -e $VimVundle ]; then
  rm -rf $VimVundle
fi
git clone git@github.com:VundleVim/Vundle.vim.git  ~/.vim/bundle/Vundle.vim
cp ./vimrc ~/.vimrc
vim +PluginInstall +qall
cp ./tex.snippets "$VimSnip/tex.snippets"
cp ./all.snippets "$VimSnip/all.snippets"
